import { computed } from '@preact/signals-core'
import { Routine } from './routine'

type Status = 'idle' | 'resting' | 'stretching'

const statusText = (status: Status): string =>
  ({ idle: 'ready', resting: 'rest', stretching: 'stretch' }[status])
const buttonText = (status: Status): string =>
  ({ idle: 'stretch!', resting: 'stop', stretching: 'stop' }[status])
const bodyClass = (status: Status): string =>
  ({ idle: '', resting: 'resting', stretching: 'stretching' }[status])

/**
 * different values that depend on the current status of a routine.
 */
export const createStatus = (routine: Routine) => {
  const status = computed((): Status => {
    if (!routine.isRunning.value) return 'idle'
    return routine.currentPeriod.value % 2 !== 0 ? 'resting' : 'stretching'
  })

  return {
    value: status.value,
    statusText: computed(() => statusText(status.value)),
    buttonText: computed(() => buttonText(status.value)),
    bodyClass: computed(() => bodyClass(status.value)),
  }
}
