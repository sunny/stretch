/**
 * given a time and how long a period is, what period are we in?
 */
export const periodFromTime = (periodDuration: number) => (timeInMs: number) =>
  Math.floor(timeInMs / periodDuration)

/**
 * given a total time and how long a period is,
 * how many seconds into the current period are we?
 */
export const timeInPeriod = (periodDuration: number) => (timeInMs: number) =>
  timeInMs - Math.floor(timeInMs / periodDuration) * periodDuration
