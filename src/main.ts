import { computed, effect, signal } from '@preact/signals-core'

import { createRoutine } from './routine'
import { createStatus } from './status'
import { initSettings } from './settings'

import './style.css'

const startButton: HTMLButtonElement = document.querySelector('#start')!
const statusDisplay: HTMLButtonElement = document.querySelector('#status')!
const cycleCounter: HTMLButtonElement =
  document.querySelector('#cycle-counter')!
const timerDisplay: HTMLTimeElement = document.querySelector('#timer')!

const routine = signal(createRoutine(7, 5000))
const status = computed(() => createStatus(routine.value))

initSettings(routine)

effect(() => {
  statusDisplay.textContent = status.value.statusText.value
  startButton.textContent = status.value.buttonText.value
  document.body.className = status.value.bodyClass.value
})

effect(() => {
  timerDisplay.textContent = routine.value.displayTime.value
  timerDisplay.dateTime = `PT${routine.value.currentTimeInMs.value / 1000}S`
})

effect(() => {
  if (!routine.value.isRunning.value) {
    cycleCounter.textContent = ''
  } else {
    cycleCounter.textContent = `${Math.floor(
      routine.value.currentPeriod.value / 2,
    )} / ${routine.value.cycles} cycles`
  }
})

startButton.addEventListener('click', () => {
  if (routine.value.isRunning.value) {
    routine.value.stop()
  } else {
    routine.value.start()
  }
})
