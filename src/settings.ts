import { Signal, effect } from '@preact/signals-core'
import { Routine, createRoutine } from './routine'

const settingsButton: HTMLButtonElement = document.querySelector('#settings')!
const settingsTemplate: HTMLTemplateElement =
  document.querySelector('#settings-template')!

export const initSettings = (routine: Signal<Routine>) => {
  effect(() => {
    if (routine.value.isRunning.value) {
      settingsButton.setAttribute('disabled', '')
    } else {
      settingsButton.removeAttribute('disabled')
    }
  })

  settingsButton.addEventListener('click', () => {
    const settingsContent = settingsTemplate.content.cloneNode(
      true,
    ) as DocumentFragment
    const dialog: HTMLDialogElement = settingsContent.querySelector('dialog')!
    document.body.append(dialog)
    dialog.showModal()

    const numCycles: HTMLInputElement = dialog.querySelector('#num-cycles')!
    const periodDuration: HTMLInputElement =
      dialog.querySelector('#period-duration')!

    numCycles.value = String(routine.value.cycles)
    periodDuration.value = String(routine.value.periodDuration)

    dialog.addEventListener('close', () => {
      document.body.removeChild(dialog)
      const newCyclesValue = Number(numCycles.value)
      const newDurationValue = Number(periodDuration.value)

      if (
        newCyclesValue !== routine.value.cycles ||
        newDurationValue !== routine.value.periodDuration
      ) {
        routine.value.stop()
        routine.value = createRoutine(newCyclesValue, newDurationValue)
      }
    })
  })
}
