import { describe, expect, test } from 'vitest'
import * as time from './time'

describe('timeInPeriod', () => {
  test('5000ms period', () => {
    const timeInPeriod = time.timeInPeriod(5000)
    expect(timeInPeriod(2000)).toBe(2000)
    expect(timeInPeriod(5000)).toBe(0)
    expect(timeInPeriod(17000)).toBe(2000)
    expect(timeInPeriod(22000)).toBe(2000)
    expect(timeInPeriod(30400)).toBe(400)
  })

  test('10000ms period', () => {
    const timeInPeriod = time.timeInPeriod(10000)
    expect(timeInPeriod(2000)).toBe(2000)
    expect(timeInPeriod(10000)).toBe(0)
    expect(timeInPeriod(17000)).toBe(7000)
    expect(timeInPeriod(22000)).toBe(2000)
    expect(timeInPeriod(30400)).toBe(400)
  })
})

describe('periodFromTime', () => {
  test('5000ms period', () => {
    const periodFromTime = time.periodFromTime(5000)
    expect(periodFromTime(2000)).toBe(0)
    expect(periodFromTime(7000)).toBe(1)
    expect(periodFromTime(10000)).toBe(2)
    expect(periodFromTime(17000)).toBe(3)
    expect(periodFromTime(22000)).toBe(4)
  })
  test('10000ms period', () => {
    const periodFromTime = time.periodFromTime(10000)
    expect(periodFromTime(2000)).toBe(0)
    expect(periodFromTime(7000)).toBe(0)
    expect(periodFromTime(10000)).toBe(1)
    expect(periodFromTime(17000)).toBe(1)
    expect(periodFromTime(22000)).toBe(2)
  })
})
