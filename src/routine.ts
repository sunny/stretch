import { Signal, batch, computed, signal } from '@preact/signals-core'
import * as time from './time'

export type Routine = {
  currentPeriod: Signal<number>
  currentTimeInMs: Signal<number>
  /**
   * total cycles before routine should end.
   */
  cycles: number
  /**
   * milliseconds in a period.
   */
  periodDuration: number
  displayTime: Signal<string>
  isRunning: Signal<boolean>
  start: () => void
  stop: () => void
}

const DEFAULT_NUM_CYCLES = 7
const DEFAULT_PERIOD_DURATION = 30000
const ONE_SECOND = 1000
const TICK_INTERVAL = 100

/**
 * create a routine. call `.start()` to run the routine.
 * call `.stop()` to end it manually and reset the routine's state.
 * a routine will run for a duration equal to `cycles * periodDuration` before ending.
 *
 * @param cycles - the number of cycles before the routine completes.
 *                 a cycle is two periods. defaults to 7.
 *
 * @param periodDuration - in milliseconds, how long each period should be.
 *                         defaults to 30000 (30 seconds).
 */
export const createRoutine = (
  cycles: number = DEFAULT_NUM_CYCLES,
  periodDuration: number = DEFAULT_PERIOD_DURATION,
): Routine => {
  // how long this routine should run for
  const totalDuration = cycles * periodDuration * 2

  const periodFromTime = time.periodFromTime(periodDuration)
  const timeInPeriod = time.timeInPeriod(periodDuration)

  const currentTimeInMs = signal(0)
  const interval: Signal<NodeJS.Timeout | null> = signal(null)

  const currentPeriod = computed(() => periodFromTime(currentTimeInMs.value))
  const displayTime = computed(
    () => `${timeInPeriod(currentTimeInMs.value) / ONE_SECOND} s`,
  )
  const isRunning = computed(() => Boolean(interval.value))

  const tick = () => {
    currentTimeInMs.value = currentTimeInMs.value + TICK_INTERVAL
    if (currentTimeInMs.value >= totalDuration) {
      routine.stop()
    }
  }

  const routine: Routine = {
    currentPeriod,
    currentTimeInMs,
    cycles,
    periodDuration,
    displayTime,
    isRunning,
    start: () => {
      interval.value = setInterval(tick, TICK_INTERVAL)
    },
    stop: () => {
      clearInterval(interval.value!)
      batch(() => {
        currentTimeInMs.value = 0
        interval.value = null
      })
    },
  }

  return routine
}
